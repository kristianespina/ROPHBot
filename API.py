import requests
import time

class API:
    def GetIPAddress(self):
        try:
            self.ip = requests.get('https://api.ipify.org').text
        except:
            return False
        return True

    def UpdateStatus(self,status):
        try:
            currentTime = int(round(time.time()))
            status['ip'] = self.ip
            if(currentTime >= self.timeNextUpdate):
                r = requests.post("http://127.0.0.1:3000/update",data=status)
                self.timeNextUpdate = currentTime + 60
                return True
        except:
            return False
        return False

    def __init__(self):
        self.timeNextUpdate = int(round(time.time()))
        self.ip = "127.0.0.1"
        self.GetIPAddress()