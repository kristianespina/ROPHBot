echo y|rmdir app.dist /s
CALL python build.py
CALL nuitka --windows-icon=icon.ico --recurse-all --show-progress --standalone app.py
xcopy CONFIG.json app.dist\ /y
xcopy CONFIG_device.json app.dist\ /y
xcopy interception.dll app.dist\ /y
xcopy FastFind.dll app.dist\ /y
xcopy virtualinput.dll app.dist\ /y
xcopy version.txt app.dist\ /y
