from log import *
from Driver import *
from Keybindings import DetermineKeyCode
import threading
import time
import json
import API
#import Crypto
from DisplayArt import DisplayASCII
from random import randint
from WindowManager import FindROClient, BringClientForward
from UAC import is_admin
import ctypes,sys
import asyncio

class AI:
    def Click(self,x,y):
        if (self.AntiPortal() == 1):
            return 0
        eventListener.QueueAttack(self.screen,x,y)
        #time.sleep(0.03)
        return 1  

    def AntiPortal(self):
        driver.RefreshFrame(self.screen)
        portalCount = driver.ColorCount(self.screen, self.config['colors']['portal'], 5)
        if(portalCount >= self.config['autoTeleport']['portalThreshold']):
            log("Warning! Found Portal. Count = {}. Teleporting...".format(portalCount),4)
            if(self.config['autoFlyWing']['whenPortalExists'] == 1):
                self.AutoTeleport(1)
                return 1
            elif (self.config['autoTeleport']['whenPortalExists'] == 1):
                self.AutoTeleport(2)
                return 1            
        return 0

    def AutoPotion(self):
        # HP Bar is 58x3 = 174 pixels total
        driver.RefreshFrameRadius(self.screen,150)
        hpPercent = (driver.ColorCount(self.screen, 0x10ef21, 0) + driver.ColorCount(self.screen, 0x10ee21, 0))/1.74
        spPercent = (driver.ColorCount(self.screen, 0x1963de, 0) + driver.ColorCount(self.screen, 0x1863de, 0))/1.74

        if(hpPercent <= self.config['autoPotion']['HP_Threshold']):
            log('HP Pots Triggered!')
            eventListener.QueueItem(self.screen, self.config['autoPotion']['HP_Hotkey'])
            time.sleep(self.config['autoPotion']['delay'])

        if(hpPercent <= self.config['autoPotion']['SP_Threshold']):
            log('SP Pots Triggered!')
            eventListener.QueueItem(self.screen, self.config['autoPotion']['SP_Hotkey'])
            time.sleep(self.config['autoPotion']['delay'])

        if(self.config['autoHeal']['enabled'] == 1 and hpPercent <= self.config['autoHeal']['HP_Threshold']):
            log('Auto-Heal Triggered!')
            eventListener.QueueAttack(self.screen,driver.clients[self.screen]['center_x'],driver.clients[self.screen]['center_y'],self.config['autoHeal']['hotkey'])
            time.sleep(self.config['autoHeal']['delay'])
        return 1
        


    def AttackMob(self,x,y):
        self.Click(x,y)
        return 1

    def SearchLoots(self,radius=0):
        lootFound,x,y = driver.FindNearestSpot(self.screen,10,self.config['colors']['loots'],radius)
        while(lootFound == True):
            lootFound,x,y = driver.FindNearestSpot(self.screen,10,self.config['colors']['loots'],radius)
            log('Picking up loot at {}, {}'.format(x,y),3)
            self.Click(x,y)
        return(lootFound,x,y)


    def CheckIfAttacking(self):
        driver.RefreshFrameRadius(self.screen,self.config['autoAttack']['dropIfNotWithinRange'])
        numMonster = driver.ColorCount(self.screen,self.config['colors']['attack'])
        if numMonster >= self.config['autoAttack']['attackingThreshold']:
            return True
        return False

    def SearchMob(self,radius=0):
        self.AutoPotion()
        self.SearchLoots(self.config['autoLoot']['lootRadius'])
        monsterFound,x,y = driver.FindNearestSpot(self.screen,10,self.config['colors']['attack'],radius)
        if(monsterFound == True):
            self.AttackMob(x,y)

            self.config['autoAttack']['retries'] += 1
            if(self.config['autoAttack']['retries'] == 1):
                log('Attacking monster at {}, {}'.format(x,y),2)
            if self.CheckIfAttacking() == True:
                self.config['autoAttack']['retries'] = 0
            if(self.config['autoAttack']['retries'] >= self.config['autoAttack']['maxTries']):
                log("Too many tries. Dropping target...",2)
                self.config['autoAttack']['retries'] = 0
                self.RandomWalk(5)

        return(monsterFound,x,y)

    def CheckBestPath(self,radius=150):
        '''
            Routine 1: Check if movement is blocked
            if blocked -> check best route
            if not -> continue current route

            returns
            @arg1 (0|1) -> 0 if continue current pathing | 1 if change path
        '''
        driver.RefreshFrameRadius(self.screen,radius)
        numObstacles = driver.ColorCount(self.screen,self.config['colors']['obstacle'])+driver.ColorCount(self.screen,self.config['colors']['cliff'])
        if(numObstacles < self.config['randomWalk']['obstacleThreshold']):
            return 1
        '''
            Kristian Path finding method: Scoring
            Quadrant Configuration
            >> [1][0]
            >> [2][3]
        '''
        #walkable_pixels = []
        bestPath_1 = 0
        bestPath_2 = 0
        bestPixels_1 = 1
        bestPixels_2 = 1
        runnerUp = 0
        for i in range(len(driver.clients[self.screen]['quadrants'])):
            driver.RefreshFrame(self.screen,driver.clients[self.screen]['quadrants'][i][0],driver.clients[self.screen]['quadrants'][i][1],driver.clients[self.screen]['quadrants'][i][2],driver.clients[self.screen]['quadrants'][i][3])
            numPixels = driver.ColorCount(self.screen,self.config['colors']['walkable'])+driver.ColorCount(self.screen,self.config['colors']['water'])
            #walkable_pixels.append(numPixels)
            if numPixels > bestPixels_1:
                bestPath_2 = bestPath_1
                bestPixels_2 = bestPixels_1
                bestPath_1 = i
                bestPixels_1 = numPixels

        # Kristian's Path-finding Algorithm
        # Score Quadrants
        x=0
        y=0
        # Allow second best route if within 60% of best direction
        # OR if previous route is the best route

        prevDir = self.randomWalk_previousDirection.pop(0)
        if bestPixels_2/bestPixels_1 >= 0.6 and prevDir == bestPath_1:
            log("Selecting second best route")
            bestPath_1 = bestPath_2

        self.randomWalk_previousDirection.append(bestPath_1)

        if bestPath_1 == 0:
            x += 1
            y -= 1
        if bestPath_1 == 1:
            x -= 1
            y -= 1
        if bestPath_1 == 2:
            x -= 1
            y += 1
        if bestPath_1 == 3:
            x += 1
            y += 1

        # Limit Movement
        if x > 1:
            x = 1
        if x < -1:
            x = -1
        if y > 1:
            y = 1
        if y < -1:
            y = -1

        self.randomWalk_score[0],self.randomWalk_score[1] = x,y
        return 3
    def AutoBuffs(self):
        currentTime = int(round(time.time()))
        for buff in self.config['autoBuffs']:
            if buff['timeLastBuff']+buff['cooldown'] < currentTime:
                time.sleep(1)
                log("Using Skill [{}]".format(buff['name']))
                eventListener.QueueBuff(self.screen,
                                        buff['name'],
                                        buff['hotkey'],
                                        True if buff['isSelfSkill'] else False,
                                        driver.clients[self.screen]['center_x'],
                                        driver.clients[self.screen]['center_y'],
                                        buff['afterCastDelay'])
                time.sleep(buff['afterCastDelay'])
                buff['timeLastBuff'] = currentTime
    def AutoTeleport(self, force=0):
        currentTime = int(round(time.time()))

        if(self.config['autoFlyWing']['whenIdle'] == 1 and self.config['autoFlyWing']['timeLastFlyWing']+self.config['autoFlyWing']['cooldown'] < currentTime) or force == 1:
            log("Auto Fly Wing...")
            self.config['autoFlyWing']['timeLastTeleport'] = currentTime
            eventListener.QueueFly(self.screen,self.config['autoFlyWing']['hotkey'])
            time.sleep(1)
            return 1

        if(self.config['autoTeleport']['timeLastTeleport']+self.config['autoTeleport']['cooldown'] < currentTime or force == 2):
            log("Auto Teleporting...")
            self.config['autoTeleport']['timeLastTeleport'] = currentTime
            eventListener.QueueFly(self.screen,self.config['autoTeleport']['hotkey'],1)
            time.sleep(1)
            return 2
        return 0

    def RandomWalk(self, forceWalk=0):
        isFound,x,y = self.SearchMob()
        if(isFound == True and forceWalk == 0):
            return 0
        else:
            self.AutoTeleport()
            if forceWalk == 0:
                forceWalk = 1
        while forceWalk > 0:
            log("Walking...")
            isChangePath = self.CheckBestPath(self.config['randomWalk']['obstacleRadius'])
            
            x = driver.clients[self.screen]['center_x']+(self.randomWalk_score[0]*self.config['randomWalk']['range'])
            y = driver.clients[self.screen]['center_y']+(self.randomWalk_score[1]*self.config['randomWalk']['range'])
            while (isChangePath > 0):
                self.Click(x,y)
                time.sleep(0.5)
                isChangePath -= 1
            forceWalk -= 1
        return 1

    def StartBot(self):
        while self.active == True:
            walking = self.RandomWalk()
            time.sleep(0.5)
            if(walking == 1):
                self.AutoBuffs()
        return 1

    def StopBot(self):
        self.active = False

    def start(self):
        log("Running AI on Client#{}".format(self.screen))
        self.active = True
        self.thread = threading.Thread(target=self.StartBot)
        self.thread.start()

    def __init__(self,screen):
        self.active = False
        self.screen = screen
        with open('CONFIG.json', 'r') as config_file:
            self.config = json.load(config_file)

        self.randomWalk_score = [1,1] #[x,y]
        self.randomWalk_previousDirection = [0,0]

        ''' auto attack '''
        self.config['autoAttack']['retries'] = 0

        ''' auto buffs '''
        for i in self.config['autoBuffs']:
            i['timeLastBuff'] = 0

        ''' auto teleport '''
        self.config['autoTeleport']['timeLastTeleport'] = 0

        ''' auto fly wing '''
        self.config['autoFlyWing']['timeLastFlyWing'] = 0



class EventListener:
    def ActivateScreen(self, screen):
        BringClientForward(driver.clients[screen]['ID'])
        return 1
    def Listen(self):
        while self.listening == True:
            self.API.UpdateStatus(driver.clientinfo)
            for action in self.queue:
                screen = action['screen']

                if (action['flyQueue']):
                    cmd = action['flyQueue'].pop(0)
                    if not len(cmd): break
                    self.ActivateScreen(screen)
                    driver.KeySend(screen, cmd['hotkey'])
                    if(cmd['cmdType'] == 1):
                        time.sleep(self.config['autoTeleport']['lagCompensation'])
                        driver.KeySend(screen, 28) 
                    time.sleep(cmd['delay'])

                if (action['buffQueue']):
                    for i in range(len(action['buffQueue'])):
                        cmd = action['buffQueue'].pop(0)
                        if not len(cmd): break
                        self.ActivateScreen(screen)
                        driver.KeySend(screen,cmd['hotkey'])
                        time.sleep(1) # Wait for Skill Ring
                        if(cmd['isSelfTarget'] == False):
                            driver.Click(screen,cmd['x'],cmd['y'])
                        time.sleep(cmd['delay'])

                if (action['itemQueue']):
                    cmd = action['itemQueue'].pop(0)
                    if not len(cmd): break
                    self.ActivateScreen(screen)
                    driver.KeySend(screen,cmd['hotkey'])
                    time.sleep(cmd['delay'])

                if (action['attackQueue']):
                    for i in range(len(action['attackQueue'])):
                        cmd = action['attackQueue'].pop(0)
                        if not len(cmd): break
                        self.ActivateScreen(screen)
                        if(cmd['hotkey']):
                            driver.KeySend(screen,cmd['hotkey'])
                            time.sleep(1) # Wait for Skill Ring
                        driver.Click(screen,cmd['x'],cmd['y'])

                time.sleep(self.config['switchDelay'])
            #print(self.queue)
            time.sleep(0.01)
        return 1
    def QueueFly(self, screen, hotkey, cmdType=0):
        queue = {
                'hotkey': hotkey,
                'cmdType': cmdType,#0 = fly wing , 1 = teleport
                'delay': 0
                }
        self.queue[screen]['flyQueue'] = [queue]
        
    def QueueAttack(self, screen, x, y, hotkey=None):
        attackQueue = {
                    'hotkey': hotkey,
                    'x': x,
                    'y': y
                }
        self.queue[screen]['attackQueue'] = [attackQueue]
        return 1

    def QueueBuff(self, screen, buffName, hotkey, isSelfTarget=True, x=0, y=0, delay=0):
        queue = {
                    'name': buffName, #Teleport can also be here
                    'hotkey': hotkey,
                    'isSelfTarget': isSelfTarget,
                    'x': x,
                    'y': y,
                    'delay': delay
                }
        self.queue[screen]['buffQueue'].append(queue)
        return 1

    def QueueItem(self, screen, hotkey):
        queue = {
                'hotkey': hotkey,
                'delay': 0
                }
        self.queue[screen]['itemQueue'] = [queue]


    def start(self):
        self.listening = True
        self.listener = threading.Thread(target=self.Listen)
        self.listener.start()
        return 1
    def stop(self):
        self.listening = False
        self.listener.stop()

    def __init__(self):
        self.queue = []
        self.API = API.API()
        with open('CONFIG.json', 'r') as config_file:
            self.config = json.load(config_file)
        for i in range(len(driver.clients)):
            self.queue.append(
                {
                    'screen': i,
                    'flyQueue': [{#Teleport/Fly Wing
                        #{
                        #    'hotkey': 'F1',
                        #    'type': 0,#0 = fly wing , 1 = teleport
                        #    'delay': 0
                        #}
                    }],
                    'buffQueue': [
                        #{
                        #    'name': 'Unknown' #Teleport can also be here
                        #    'hotkey': 'F1',
                        #    'isSelfTarget': False,
                        #    'x': x,
                        #    'y': y,
                        #    'delay': 0
                        #}
                    ],
                    'itemQueue': [{#For HP/SP Potion Use
                        #{
                        #    'hotkey': 'F1',
                        #    'delay': 0
                        #}
                    }],
                    'attackQueue': [{
                        #'hotkey': None,
                        #'x': x,
                        #'y': y
                    }]
                }
            )
        self.start()

if is_admin():
    DisplayASCII()
    driver = InitializeDriver()
    eventListener = EventListener()
    #FindROClient('Ragnarok')

    log("Found {} {}...".format(len(driver.clients), "clients" if len(driver.clients)>1 else "client"))
    bots = []

    time.sleep(2)
    for screen_id in range(len(driver.clients)):
        bots.append(AI(screen_id))

    for bot in bots:
        bot.start()


else:
    # Re-run the program with admin rights
    ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, __file__, None, 1)     
