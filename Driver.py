from WindowManager import *
from ctypes import *
from win32api import GetSystemMetrics
from iBus import Bus
import json

class InitializeDriver:        
    def RefreshFrame(self,screen,left=0,top=0,right=0,bottom=0):
        if(right == 0):
            right = self.clients[screen]['boundaries'][2]
        if(bottom == 0):
            bottom = self.clients[screen]['boundaries'][3]
        return self.dll.ROSnap(left,top,right,bottom,screen)

    def RefreshFrameRadius(self,screen,radius=200):
        return self.RefreshFrame(
                                screen,
                                self.clients[screen]['center_x']-radius,
                                self.clients[screen]['center_y']-radius,
                                self.clients[screen]['center_x']+radius,
                                self.clients[screen]['center_y']+radius
                                )

    def ColorCount(self,screen,color,variation=0):
        return self.dll.ROColorCount(int(color),screen,variation)

    def Click(self,screen,x,y):
        return(self.dll.MouseClick(self.device_mouse,int(x),int(y),self.monitor['width'],self.monitor['height'],self.emulationDelay,screen))
    
    def KeySend(self,screen,keyCode):
        return(self.dll.KeyboardSend(self.device_keyboard, keyCode, self.emulationDelay,screen))
    
    def GenericColorSearch(self,sizeSearch,matchMin,x,y,color,variation,screen = 0):
        return(self.dll.ROGenericColorSearch(sizeSearch,matchMin,x,y,color,variation,screen))

    def FindNearestSpot(self, screen, matchMin, color, radius=0):
        if(radius>0):
            self.RefreshFrameRadius(screen, radius)
        else:
            self.RefreshFrame(screen)
        _x = c_int(self.clients[screen]['center_x'])
        _y = c_int(self.clients[screen]['center_y'])
        _matchMin = c_int(matchMin)
        isFound = self.dll.ROGenericColorSearch(self.sizeSearch, byref(_matchMin), byref(_x), byref(_y), color, 0, screen)
        return(isFound,_x.value,_y.value)

    def listClients(self):
        clients = []
        for clientID in EnumerateROClient():
            boundary = GetWindowRect(clientID)
            center_x = int((boundary[0]+boundary[2])/2)
            center_y = int((boundary[1]+boundary[3])/2)
            clients.append({
                'ID': clientID,
                'boundaries': boundary,
                'center_x': center_x,
                'center_y': center_y,
                'quadrants':[
                            [center_x,center_y-150,center_x+150,center_y],#top right
                            [center_x-150,center_y-150,center_x,center_y],#top left
                            [center_x-150,center_y,center_x,center_y+150],#bottom left
                            [center_x,center_y,center_x+150,center_y+150],#bottom right
                            ]
            })
        return clients

    def __init__(self):
        self.interface = Bus()
        self.bus,self.clientinfo = self.interface.Retry()
        self.clients = self.listClients()
        self.dll = cdll.LoadLibrary('virtualinput.dll')
        if(self.bus >= 1):
            self.dll.InitializeArcane()
        self.monitor = {
            'width': GetSystemMetrics(0),
            'height': GetSystemMetrics(1)
        }
        with open('CONFIG_device.json', 'r') as config_file:
            config = json.load(config_file)

        self.sizeSearch = 50
        self.device_mouse = config['mouse']
        self.device_keyboard = config['keyboard']
        self.emulationDelay = config['emulationDelay']
