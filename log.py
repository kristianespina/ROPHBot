'''
    Types
    0 = Red
    1 = Green

'''
from colorama import init,Fore,Back,Style
init()
def log(text,type=0):
    if(type == 0):
        print(Fore.GREEN + '[ INFO ] '+ Style.RESET_ALL + text)
    elif(type == 1):
        print(Fore.RED + '[ ERROR ] '+ Style.RESET_ALL + text)
    elif(type == 2):
        print(Fore.CYAN + '[ MOB ] ' + Style.RESET_ALL + text)
    elif(type == 3):
        print(Fore.MAGENTA + '[ LOOTS ] ' + Style.RESET_ALL + text)
    elif(type == 4):
        print(Fore.YELLOW + '[ WARNING ] ' + Style.RESET_ALL + text)
    return