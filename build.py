'''
    Title: Get the current git hash in a Python script
    Taken from https://stackoverflow.com/questions/14989858/get-the-current-git-hash-in-a-python-script
'''
import subprocess
def get_git_revision_hash():
    return subprocess.check_output(['git', 'rev-parse', 'HEAD'])

file = open('version.txt', 'w')
file.write(str(get_git_revision_hash().decode('UTF-8').rstrip()))
file.close