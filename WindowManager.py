#from win32 import win32gui
from win32gui import *
from time import sleep
from colorama import init,Fore,Back,Style
from log import *
import re

def FindROClient(title):
    log("Scanning  Client. Please wait...")
    ragnarokhWnd = FindWindow(None, title)
    if(ragnarokhWnd != 0):
        log("Bringing to foreground!")
        try:
            ShowWindow(ragnarokhWnd, 5)
            SetForegroundWindow(ragnarokhWnd)
        except:
            log("Insufficient access rights!",2)
            log("Please run EZ Bot as Administrator!",2)
    else:
        log("Cannot find Ragnarok Client...",1)
        log("Please launch Ragnarok Client before starting the bot...",1)
        sleep(10)
    return ragnarokhWnd
def BringClientForward(ragnarokhWnd):
    try:
        ShowWindow(ragnarokhWnd, 5)
        SetForegroundWindow(ragnarokhWnd)
    except:
        log("Cannot set window to foreground!",2)
    return

def AppendROClient(hWnd, clientList):
    if(GetWindowText(hWnd) == "Ragnarok" or GetWindowText(hWnd) == "Ragnarok | Gepard Shield 3.0 (^-_-^)"):
    #if re.search(r"Ragnarok",GetWindowText(hWnd)):
        log("Found RO Client: {}".format(hWnd))
        clientList.append(hWnd)
    return

def EnumerateROClient():
    clientList = []
    log ("Searching for Ragnarok Online Client...")
    EnumWindows(AppendROClient, clientList)
    return clientList
