import base64
import win32api
import json
# Cryptography
import os
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import padding
padder = padding.PKCS7(128).padder()
backend = default_backend()
key = b"29Pvp4my77inzlG044jXEn0mD5GiLx4S"
iv = b"R7wKNWlIOdzRS5VD"
cipher = Cipher(algorithms.AES(key), modes.CBC(iv), backend=backend)

class Bus:
    def Retry(self):
        try:
            file_in = open(base64.b64decode("ZXh1dGlscy5kbGw="), "rb")
            decryptor = cipher.decryptor()
            dtext = decryptor.update(file_in.read()) + decryptor.finalize()
            unpadder = padding.PKCS7(128).unpadder()
            unpadded_text = unpadder.update(dtext)
            unpadded_text += unpadder.finalize()

            clientinfo = json.loads(unpadded_text)
            if(str(self.hardware[1]) == clientinfo['id']):
                return int(clientinfo['state']), clientinfo
            else:
                return 0, self.dummy
        except:
            return 0, self.dummy

    def __init__(self):
        self.hardware = win32api.GetVolumeInformation("C:\\")
        self.dummy = {
            'name': 'Unknown User',
            'id': self.hardware[1],
            'state': 0,
            'ip': '127.0.0.1'
        }