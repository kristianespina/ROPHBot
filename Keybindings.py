keybindings = {
    "F1": 59,
    "F2": 60,
    "F3": 61,
    "F4": 62,
    "F5": 63,
    "F6": 64,
    "F7": 65,
    "F8": 66,
    "F9": 67,
    "Q": 16,
    "W": 17,
    "E": 18,
    "R": 19,
    "T": 20,
    "Y": 21,
    "U": 22,
    "I": 23,
    "O": 24,
    "P": 25,
    "[": 26,
    "]": 27,
    "A": 30,
    "S": 31,
    "D": 32,
    "F": 33,
    "G": 34,
    "H": 35,
    "J": 36,
    "K": 37,
    "L": 38,
    ";": 39,
    "'": 40,
    "Z": 44,
    "X": 45,
    "C": 46,
    "V": 47,
    "B": 48,
    "N": 49,
    "M": 50,
    ",": 51,
    ".": 52,
    "/": 53,
    "NUMPAD_1": 79,
    "NUMPAD_2": 80,
    "NUMPAD_3": 81,
    "NUMPAD_4": 75,
    "NUMPAD_5": 76,
    "NUMPAD_6": 77,
    "NUMPAD_7": 71,
    "NUMPAD_8": 72,
    "NUMPAD_9": 73,
}
def DetermineKeyCode(keyCode):
    if type(keyCode) == int:
        return(keyCode)
    else:
        if keyCode.upper() in keybindings:
            return( keybindings[keyCode.upper()] )
        else:
            print("Unknown Hotkey!")
    return False