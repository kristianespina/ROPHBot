import os
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import padding
padder = padding.PKCS7(128).padder()
backend = default_backend()
key = b"29Pvp4my77inzlG044jXEn0mD5GiLx4S"
iv = b"R7wKNWlIOdzRS5VD"
cipher = Cipher(algorithms.AES(key), modes.CBC(iv), backend=backend)

plaintext = b'{"name": "123", "id": "123", "state": "123"}'
padded_text = padder.update(plaintext)
padded_text += padder.finalize()

encryptor = cipher.encryptor()

encryptext = encryptor.update(padded_text) + encryptor.finalize()


decryptor = cipher.decryptor()
dtext = decryptor.update(encryptext) + decryptor.finalize()
unpadder = padding.PKCS7(128).unpadder()
unpadded_text = unpadder.update(dtext)
unpadded_text += unpadder.finalize()
print(unpadded_text)


