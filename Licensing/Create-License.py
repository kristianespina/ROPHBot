import uuid
import win32api
import json
# Cryptography
import os
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import padding
padder = padding.PKCS7(128).padder()
backend = default_backend()
key = b"29Pvp4my77inzlG044jXEn0mD5GiLx4S"
iv = b"R7wKNWlIOdzRS5VD"
cipher = Cipher(algorithms.AES(key), modes.CBC(iv), backend=backend)


# Get MAC Address
hardware = win32api.GetVolumeInformation("C:\\")
hardware_id = hardware[1]
print(hardware_id)

def createLicense(info):
    bytedata = info.encode()
    padded_text = padder.update(bytedata)
    padded_text += padder.finalize()

    encryptor = cipher.encryptor()
    encryptext = encryptor.update(padded_text) + encryptor.finalize()

    file_out = open("exutils.dll", "wb")
    file_out.write(encryptext)

    

def checkLicense():
    file_in = open("exutils.dll", "rb")
    decryptor = cipher.decryptor()
    dtext = decryptor.update(file_in.read()) + decryptor.finalize()
    unpadder = padding.PKCS7(128).unpadder()
    unpadded_text = unpadder.update(dtext)
    unpadded_text += unpadder.finalize()
    print(unpadded_text)
    return unpadded_text

print("Please fill up the following information:")
name = input("Client's Name: ")
mac = input("Client's Mac Address: ")
state = input("State (0 = unlicensed, 1 = single client, 2+ = multiclient): ")
client_info = {
    'name': name,
    'id': mac,
    'state': state
}
createLicense(json.dumps(client_info))
print("Done! > exutil.dll")

#print(checkLicense().decode('utf-8'))
