from Crypto.Cipher import AES
import base64
import win32api

class Licensing:
    def checkLicense(self):
        key = bytes('GXCo2h6vZuIX0oP9', 'utf-8')
        file_in = open(base64.b64decode("bXVsdGkuYmlu"), "rb")
        nonce, tag, ciphertext = [ file_in.read(x) for x in (16, 16, -1) ]
        cipher = AES.new(key, AES.MODE_EAX, nonce)
        hppot = cipher.decrypt_and_verify(ciphertext, tag)
        hardware = win32api.GetVolumeInformation("C:\\")
        if(str(hardware[1]) == hppot.decode('utf-8')):
            return 0
        else:
            return 1
