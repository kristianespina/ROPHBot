from WindowManager import *
from log import *
from win32api import GetSystemMetrics

class CreateInterface:
    def listClients(self):
        clients = []
        for clientID in EnumerateROClient():
            boundary = GetWindowRect(clientID)
            clients.append({
                'ID': clientID,
                'boundaries': boundary,
                'center_x': int((boundary[0]+boundary[2])/2),
                'center_y': int((boundary[1]+boundary[3])/2)
            })
        return clients

    def __init__(self):
        self.clients = self.listClients()
        print(self.clients)