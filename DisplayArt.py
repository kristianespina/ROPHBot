from log import *
from colorama import Fore, Back, Style

def DisplayASCII():
    versionFile = open('version.txt', 'r')
    print(Style.RESET_ALL+"------------------------------------------------------------------")
    log(' ________  ________        _______               __     ')
    log('/        |/        |      /       \             /  |    ')
    log('$$$$$$$$/ $$$$$$$$/       $$$$$$$  |  ______   _$$ |_   ')
    log('$$ |__        /$$/        $$ |__$$ | /      \ / $$   |  ')
    log('$$    |      /$$/         $$    $$< /$$$$$$  |$$$$$$/   ')
    log('$$$$$/      /$$/          $$$$$$$  |$$ |  $$ |  $$ | __ ')
    log('$$ |_____  /$$/____       $$ |__$$ |$$ \__$$ |  $$ |/  |')
    log('$$       |/$$      |      $$    $$/ $$    $$/   $$  $$/ ')
    log('$$$$$$$$/ $$$$$$$$/       $$$$$$$/   $$$$$$/     $$$$/  ')
    log('Version: '+Fore.CYAN+'{}'.format(versionFile.read()))
    print(Style.RESET_ALL+"------------------------------------------------------------------")
    versionFile.close()
    return True